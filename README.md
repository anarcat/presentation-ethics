These slides were made in a talk based on [this article I wrote after
Kubecon Europe 2018](https://anarc.at/blog/2018-05-26-kubecon-rant/) and reuses more or less the same ideas.

The [presentation](presentation.md) should be viewable online, but it might take
time to load because those images are the original out of the camera
and are huge. I didn't bother to resize. The [PDF version](presentation.pdf) is
similarly used and was used to create a usable slideshow.

Technical details
-----------------

I built those slides in a rush about one hour before the talk, and I
designed the talking points about two hours before, based on the
article that was published in the same week. Probably the shortest
lifecycle of such a talk I've ever did.

I had trouble with tooling, which I'll expand on here.

One problem with this presentation was to figure out how to show it. I
thought up the whole article visual style inspired by [Maciej
Cegłowski ](http://idlewords.com/talks/) and wanted to just show one large image per slide.

This was surprisingly hard. My previously prefered slideshow tool,
[pinpoint](https://wiki.gnome.org/Apps/Pinpoint) now has all sorts of issues. I was already having
trouble with it before: I had a custom-built copy in
`/usr/local/bin/pinpoint` based on [my fork](https://github.com/anarcat/pinpoint/) which I never got
around to clear up. My fork itself was based on other people's commits
and had diverged from upstream. So I reverted to whatever was in
Debian stable (stretch, so 1:0.1.8-2+b1). That just completely
crashes: core dump after the thing gets loaded. Grah. Maybe my images
were too big? In any case, the project unfortunately seems
[abandoned](https://mail.gnome.org/archives/pinpoint-list/2017-July/msg00001.html) as everyone seems to have fled to Mac OS. Last commit,
all forks combined, is two years old. Guh.

Next I tried [sent](https://tools.suckless.org/sent/). Looks promising, same principle: small text
file, simple stuff. But looking at their preview, the images don't
scale to the whole width, so discarded.

Next I went back to [Pandoc](https://pandoc.org/) because I had been using it all week
for a technical report. I remember reading about how it does
slides. So I first tried rendering to HTML:

    pandoc --to s5 --standalone presentation.mdwn --ouput presentation.html

Results were just ridiculous: each image was shown unscaled, 100%,
pixel-perfect so you can't see anything. Total crap. It seems like the
HTML/CSS/whatever just doesn't kick in at all. I tried other output
formats like [Slidy](https://www.w3.org/Talks/Tools/Slidy2/#(1)) or [Reveal.js](https://revealjs.com/). In theory, the latter
supports image backgrounds, but out of the box the output is as bad as
[s5](https://meyerweb.com/eric/tools/s5/). I would need to use the `{data-background-image="image.jpg"}`
parameter there but hadn't figured it out in time for the
talk. [DZSlides](http://paulrouget.com/dzslides/) is also pretty neat, but out of the box the
overlay is huge and doesn't contrast correctly. In all those cases, we
might end up needing the `raw_html` extension, which makes me wonder
why I would want to use Pandoc in the first place. I guess the
resulting HTML file can also be tweaked by hand - using Markdown as a
way to generate a template file...

I ended up using beamer because it worked out of the box and created a
PDF I could use portably. I just used whatever default settings Pandoc
uses. I liked that it created subtitles for my snarky comments, but I
would have prefered if it was full screen with an overlay. There seems
to be a way to [make images full size in beamer](https://tex.stackexchange.com/questions/3915/image-on-full-slide-in-beamer-package) but I haven't got
the hang out of messing with LaTeX templates in Pandoc well enough to
tackle that yet. Besides, the [Beamer user guide](http://ctan.math.utah.edu/ctan/tex-archive/macros/latex/contrib/beamer/doc/beameruserguide.pdf) is about a
billion pages long (not true: 243 pages) and I'm not sure I want to go
through all of that mess.

One thing I missed was a way to have notes on a side screen: this is
usually possible with [pdfpc](https://pdfpc.github.io/) but I recently switched to [i3](https://i3wm.org/)
and was having trouble just getting a normal PDF viewer display full
screen on the projector without turning my laptop screen off, so I
didn't look at that more deeply. [Easy Lecture Slides Made Difficult
with Pandoc and Beamer](https://andrewgoldstone.com/blog/2014/12/24/slides/) (best title ever) has notes on how to
handle... well, notes, but also fonts, colors and, in general, not
have LaTeX look like LaTeX and its "*instantly-recognizable
blah-ness*". His resulting setup is a Lovecraft-ian horror, but it
seems to work. You should probably run away screaming instead. He also
refers to an [easier guide](http://benschmidt.org/2014/11/07/building-outlines-for-markdown-documents-with-pandoc/) but that one has Haskell plugins in it
and also makes me want to scream.

So yeah, yet another unsolved problem space, sigh.

Oh, and one last thing I considered is to just show frigging images
one by one. You know, like hitting F5 in whatever your favorite image
viewer is. That also works, after all. [Geeqie](http://geeqie.org/) is my favorite
image viewer these days, but there's also GNOME's [eog](https://wiki.gnome.org/Apps/EyeOfGnome/), [pho](http://shallowsky.com/software/pho/),
[feh](https://feh.finalrewind.org/), [fim](https://www.nongnu.org/fbi-improved/), [sxiv](https://github.com/muennich/sxiv), and who knows what else, you'll probably figure
*you* can write your own frigging image viewer after reading
this. Downside is, obviously, not side notes and you get to build the
images yourself if you want overlay.

Other options that I have look at and discarded:

 * [Darkslide](https://github.com/ionelmc/python-darkslide) - also HTML-based, but no full screen images
 * [mdp](https://github.com/visit1985/mdp) - a console-only tool, probably totally inappropriate for
   this
 * [Koumbit wiki](https://wiki.koumbit.net/OutilsDePr%C3%A9sentation) - my old source for this
