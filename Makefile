all: presentation.pdf

%.pdf: %.md
	pandoc -t beamer -s presentation.md -o presentation.pdf
