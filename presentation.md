# Diversité, éducation, privilège et éthique en TIC

----

![4000 hommes blancs](DSCF3307.JPG)

----

![Programmes de diversité](DSCF3502.JPG)

----

![En extase au Home Depot](home-depot-speed.png)

----

![USAF saves (money)!](usaf-cost-savings.png)

----

!["Left to his own devices he couldn’t build a toaster. He could just
about make a sandwich and that was it." -- Mostly Harmless, Douglas
Adams, 1992](toaster-project.jpg)

----

![Un homme blanc soigné par une femme blanche.](DSCF3129.JPG)

----

![Nothing is like corporate nothing](DSCF3484.JPG)

----

![CAPTCHA collatéraux](recaptcha.png)

----

![Locutus of Borg](Locutus-of-Borg.jpg)

----

![...mais que créé-t-on vraiment?](DSCF3496.jpg)

----

Présentation basée sur cet article:

<https://anarc.at/blog/2018-05-26-kubecon-rant/>

Voir aussi:

 * [The Trouble with charitable billionaires](https://www.theguardian.com/news/2018/may/24/the-trouble-with-charitable-billionaires-philanthrocapitalism) (The Guardian)
 * [The Birth of the New American Aristocracy](https://www.theatlantic.com/amp/article/559130/) (The Atlantic)
 * [La coop que j'ai co-fondé](https://koumbit.org) (<https://koumbit.org>)
